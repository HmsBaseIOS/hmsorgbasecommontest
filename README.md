# HmsBaseComponentTest

[![CI Status](https://img.shields.io/travis/虞振兴/HmsBaseComponentTest.svg?style=flat)](https://travis-ci.org/虞振兴/HmsBaseComponentTest)
[![Version](https://img.shields.io/cocoapods/v/HmsBaseComponentTest.svg?style=flat)](https://cocoapods.org/pods/HmsBaseComponentTest)
[![License](https://img.shields.io/cocoapods/l/HmsBaseComponentTest.svg?style=flat)](https://cocoapods.org/pods/HmsBaseComponentTest)
[![Platform](https://img.shields.io/cocoapods/p/HmsBaseComponentTest.svg?style=flat)](https://cocoapods.org/pods/HmsBaseComponentTest)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

HmsBaseComponentTest is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'HmsBaseComponentTest'
```

## Author

虞振兴, yuzhenxing@hptown.cn

## License

HmsBaseComponentTest is available under the MIT license. See the LICENSE file for more info.
