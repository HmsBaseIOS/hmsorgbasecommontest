//
//  UIImage+HPDBaseCommon.swift
//  HmsBaseComponent
//
//  Created by 虞振兴 on 2023/8/14.
//

import Foundation
import UIKit

public extension UIImage {
    
    //拉伸图片 固定拉伸某部分区域 从而达到拉伸不变形效果
    func stretchImageWithEdgeInset(edgeInset:UIEdgeInsets = .zero) -> UIImage? {
        //设置拉伸图片时，需要用来填充拉伸位置的部分
        let insets = edgeInset == .zero ? UIEdgeInsets(top: (self.size.height)/2 + 10, left: 10, bottom: 10, right: 10) : edgeInset
         // 指定为拉伸模式，伸缩后重新赋值
     
        let image = self.resizableImage(withCapInsets: insets, resizingMode: .stretch)
   
       return image
    }
}
