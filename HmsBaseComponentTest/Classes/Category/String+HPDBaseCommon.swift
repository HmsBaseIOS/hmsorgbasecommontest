//
//  String+HPDBaseCommon.swift
//  HmsBaseComponent
//
//  Created by 虞振兴 on 2023/8/14.
//

import Foundation
import CommonCrypto  //MD5需要的库

public extension String {
    var floatValue: Float {
        return (self as NSString).floatValue
    }
    
    func isNilOrEmpty() -> Bool{
        return allSatisfy({$0.isWhitespace})
    }
    
    ///MD5 加密
    //- Returns: 32 位大写
    func md5() ->String {
        let str = self.cString(using: .utf8)
        let strLen = CUnsignedInt(self.lengthOfBytes(using: .utf8))
        let digestLen = Int(CC_MD5_DIGEST_LENGTH)
        let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity:digestLen)
        CC_MD5(str!, strLen, result)
        
        let hash = NSMutableString()
        for i in 0..<digestLen {
            hash.appendFormat("%02X", result[i])
        }
        result.deallocate()
        
        return String(format: hash as String)
    }
    
    //检测 字符串是否是网络地址
    func isHttpStr() -> Bool {
        //空
        if(self.isNilOrEmpty()){
            return false
        }
        if(self.hasPrefix("http") || self.hasPrefix("https")){
            return true
        }
        
        return false
    }
    
    //字符串截取的方法
    func substring(from index: Int , to:Int) -> String {
        if self.count > index {
            let startIndex = self.index(self.startIndex, offsetBy: index)
            let endIndex = self.index(self.startIndex, offsetBy: to)
            let subString = self[startIndex..<endIndex]
            
            return String(subString)
        } else {
            return self
        }
    }
    
    //版本字符串比较
    func versionCompare(newVersion: String) -> ComparisonResult {
        let currentArr = self.components(separatedBy: ".")
        let newArr = newVersion.components(separatedBy: ".")
        for i in 0..<currentArr.count {
            let tempInt:Int = Int(currentArr[i])!
            let tempNewVersionInt:Int = Int(newArr[i])!
            if(tempInt<tempNewVersionInt){
                return ComparisonResult.orderedAscending
            }else if(tempInt>tempNewVersionInt){
                return ComparisonResult.orderedDescending
            }
        }
        return ComparisonResult.orderedSame
    }
    
    //手机号码 中间4位改为****号
    func phoneChangeToSafe() -> String {
        let tempStr = self as NSString
        if tempStr.length > 7 {
            let nsRange = NSRange.init(location: 3, length: 4)
            let newStr = tempStr.replacingCharacters(in: nsRange, with: "****")
            return newStr
        }
        return self
    }
    
    //富文本 间距默认
    func attributedStringForLineSpacing(lineSpacing:CGFloat = 5) -> NSMutableAttributedString{
        if(self.isNilOrEmpty()){
            return NSMutableAttributedString.init()
        }
        let atti = NSMutableAttributedString(string: self)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineBreakMode = NSLineBreakMode.byTruncatingTail
        paragraphStyle.lineSpacing = lineSpacing
        atti.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, self.count))
        return atti
    }
    
    //富文本  用于有些地方的部分文字变色需求 目前只是匹配一次的需求
    //lineSpacing行间距 默认5
    func attributedStringWithColorNew(mulStr:NSAttributedString? = nil, _ strings: [String], color: UIColor,lineBreakMode:NSLineBreakMode = .byTruncatingTail,lineSpacing:CGFloat = 5) -> NSMutableAttributedString {
        let attributedString = mulStr == nil ? NSMutableAttributedString(string: self) : NSMutableAttributedString.init(attributedString: mulStr!)
        for string in strings {
            let pattern = string
            let range = attributedString.string.range(of:pattern)// 操作
            let rangeNew = attributedString.string.toNSRange(range!)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: rangeNew)
        }
        
        //        guard let characterSpacing = characterSpacing else {return attributedString!}
        //        attributedString?.addAttribute(NSAttributedString.Key.kern, value: characterSpacing, range: NSRange(location: 0, length: attributedString!.length))
        /// 设置间距
        let style = NSMutableParagraphStyle()
        /// 间隙
        style.lineSpacing = lineSpacing
        //字符串截取规则
        style.lineBreakMode = lineBreakMode
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: NSMakeRange(0, attributedString.string.count))
        
        return attributedString
    }
    
    
    //Range转换为NSRange
    func toNSRange(_ range: Range<String.Index>) -> NSRange {
        guard let from = range.lowerBound.samePosition(in: utf16), let to = range.upperBound.samePosition(in: utf16) else {
            return NSMakeRange(0, 0)
        }
        return NSMakeRange(utf16.distance(from: utf16.startIndex, to: from), utf16.distance(from: from, to: to))
    }
    
    
    ///  根据设置个数判断 数字是否有通过
    /// - Parameter textF: 输入框
    /// - Parameter range: 输入位置
    /// - Parameter importStr: 输入字符
    /// - Parameter prePointCount: 小数点前个数(默认5)
    /// - Parameter behindPointCount: 小数点后个数(默认6)
    func checkTextF(textF: UITextField, changeCharactersInRange range: NSRange, importStr: String, prePointCount: Int = 5, behindPointCount: Int = 6) -> Bool {
        if importStr.isEmpty == true {
            return true
        }
        if textF.text?.contains(".") == true { // 只能输入一个.
            if importStr == "." {
                return false
            }
            if textF.text?.count ?? 0 >= prePointCount + behindPointCount + 1 {
                //                print("没有小数点 不能输入超过限额")
                return false
            }
            // 判断小数点位数
            guard let ran = textF.text?.range(of: ".") else { return false}
            
            guard let ran1 = textF.text?.toNSRange(ran) else { return false}
            if range.location > ran1.location {//小数点后边输入
                if (textF.text?.count ?? 0) - ran1.location == behindPointCount + 1 {
                    return false
                }
                return true
            }else {//小数点前边输入
                if (textF.text?.count ?? 0) >= prePointCount + behindPointCount + 1 {
                    //                    print("没有小数点 不能输入超过限额 ");
                    return false
                }
                return true
            }
            
        }else {//无小数
            if textF.text?.count ?? 0 >= prePointCount {
                if importStr == "." {
                    return true
                }
                if importStr == "" {
                    return true
                }
                return false
            }
        }
        
        if textF.text == "0" {//0后面只能输入.
            return importStr == "." ?  true : false
        }
        if importStr == " " {
            return false
        }
        
        return true
    }
    
}

//这个方法会把 那种纯空格的文本也作为空字符串处理
extension Optional where Wrapped == String{
    var isNilOrEmpty : Bool{
        return self?.isNilOrEmpty() ?? true
    }
}
