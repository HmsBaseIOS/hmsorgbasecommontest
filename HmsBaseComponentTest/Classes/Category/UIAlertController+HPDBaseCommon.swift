//
//  UIAlertController+Common.swift
//  HPDHTownOrgApp
//
//  Created by 虞振兴 on 2023/2/28.
//

import UIKit
 
public extension UIAlertController {
    //在指定视图控制器上弹出普通消息提示框
    static func showAlert(message: String, in viewController: UIViewController) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "确定", style: .cancel))
        viewController.present(alert, animated: true)
    }
     
    //在根视图控制器上弹出普通消息提示框
    static func showAlert(message: String) {
        if let vc = UIApplication.shared.keyWindow?.rootViewController {
            showAlert(message: message, in: vc)
        }
    }
     
    //在指定视图控制器上弹出确认框
    static func showConfirm(message: String,cancelStr:String = "取消",sureStr:String = "确定", in viewController: UIViewController,
                            confirm: ((UIAlertAction)->Void)?) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: cancelStr, style: .cancel))
        alert.addAction(UIAlertAction(title: sureStr, style: .default, handler: confirm))
        viewController.present(alert, animated: true)
    }
     
    //在根视图控制器上弹出确认框
    static func showConfirm(message: String,cancelStr:String = "取消",sureStr:String = "确定", confirm: ((UIAlertAction)->Void)?) {
        if let vc = UIApplication.shared.keyWindow?.rootViewController {
            showConfirm(message: message,cancelStr: cancelStr,sureStr: sureStr, in: vc, confirm: confirm)
        }
    }
}
