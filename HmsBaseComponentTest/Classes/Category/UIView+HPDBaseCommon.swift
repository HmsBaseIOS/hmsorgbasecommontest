//
//  UIView+HPDBaseCommon.swift
//  HmsBaseComponent
//
//  Created by 虞振兴 on 2023/8/14.
//

import Foundation
import UIKit

public extension UIView{
    //Colors：渐变色色值数组 isFromTop是否是从上到下渐变 false的话 就是从左到右了
    func setLayerColors(_ colors:[CGColor], isFromTop:Bool)  {
            let layer = CAGradientLayer()
            layer.frame = bounds
            layer.colors = colors
            layer.startPoint = CGPoint(x: 0, y: 0)
        //三元运算符 condition ? X : Y 如果 condition 为 true ，值为 X ，否则为 Y
        layer.endPoint = isFromTop ? CGPoint(x: 0, y: 1):CGPoint(x: 1, y: 0)
            self.layer.addSublayer(layer)
    }
      
    /// 圆角设置
     ///
     /// - Parameters:
     ///   - view: 需要设置的控件
     ///   - corner: 哪些圆角
     ///   - radii: 圆角半径
     /// - Returns: layer图层
     func configRectCorner(view: UIView, corner: UIRectCorner, radii: CGSize) -> CALayer {
         
         let maskPath = UIBezierPath.init(roundedRect: view.bounds, byRoundingCorners: corner, cornerRadii: radii)
         
         let maskLayer = CAShapeLayer.init()
         maskLayer.frame = view.bounds
         maskLayer.path = maskPath.cgPath
         
         return maskLayer
     }
    
 
 
    //清空删除所有子视图（子元素）
    func clearSubViews() {
        for v in self.subviews as [UIView] {
            v.removeFromSuperview()
        }
    }
    
   
}
