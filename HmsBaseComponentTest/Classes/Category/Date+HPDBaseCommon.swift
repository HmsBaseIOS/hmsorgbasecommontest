//
//  Date+HPDBaseCommon.swift
//  HmsBaseComponent
//
//  Created by 虞振兴 on 2023/8/14.
//

import Foundation
public extension Date {

    var isToday: Bool {
        return Calendar.current.isDateInToday(self)
    }

    var isYesterday: Bool {
        return Calendar.current.isDateInYesterday(self)
    }

    var isTomorrow: Bool {
        return Calendar.current.isDateInTomorrow(self)
    }
    
    // yyyy-MM-dd
    func timeStringWithDataTimeToDate(format:String = "yyyyMMdd") -> String {
        //获取当前时间
        let now = self
        // 创建一个日期格式器
        let dformatter = DateFormatter()
        dformatter.dateFormat = format
        //当前时间的时间戳
        let string = dformatter.string(from:NSDate.init(timeIntervalSince1970: now.timeIntervalSince1970) as Date)
//        print("当前时间的时间：\(string)")
        return string
    }
    
    //获取时间戳
    func timeCurrentIntervalInt () -> Int {
        //获取当前时间
        let now = self
        //当前时间的时间戳
        let timeInterval:TimeInterval = now.timeIntervalSince1970
        let timeStamp = Int(timeInterval)
//        print("当前时间的时间戳：\(timeStamp)")
        return timeStamp
    }

    //获取当前日期所在周 7天日期数据
    static func getCurrentWeekDays(firstDayOfWeek: HPDBaseDayOfWeek?=nil) -> [Date] {
        var calendar = Calendar.current
        calendar.firstWeekday = (firstDayOfWeek ?? .Sunday).rawValue
        let today = calendar.startOfDay(for: Date())
        let dayOfWeek = calendar.component(.weekday, from: today)
        let weekdays = calendar.range(of: .weekday, in: .weekOfYear, for: today)!
        let days = (weekdays.lowerBound ..< weekdays.upperBound).compactMap { calendar.date(byAdding: .day, value: $0 - dayOfWeek, to: today) }
        return days
    }
    
    //获取当前日期所在周 在内的 35天日期数据 5周数据
    //weekCount 几周数据
    //firstDateIndex 用于定位的  比如-7
    static func getCurrentWeekDaysAndCount(firstDayOfWeek: HPDBaseDayOfWeek?=nil,weekCount:Int,firstDateIndex:Int) -> [NSDictionary] {
        var calendar = Calendar.current
        calendar.firstWeekday = (firstDayOfWeek ?? .Sunday).rawValue
        let today = calendar.startOfDay(for: Date())
        let dayOfWeek = calendar.component(.weekday, from: today)
//        let weekdays = calendar.range(of: .weekday, in: .weekOfYear, for: today)!
//        print("weekdays----weekdays.lowerBound--\(weekdays) --- \(weekdays.lowerBound) --- \(weekdays.upperBound)")
        //
//        let days = (1 ..< (weekCount*7+1)).compactMap { calendar.date(byAdding: .day, value: $0 - (weekCount*7 - 7 + dayOfWeek), to: today) }
        let firstDaysInt = firstDateIndex+1
        let lastDaysInt = (weekCount*7+1+firstDateIndex)
        let days = (firstDaysInt ..< lastDaysInt).compactMap {
            let toTodayCount = $0 - (weekCount*7 - 7 + dayOfWeek)
            let tempDate = calendar.date(byAdding: .day, value: $0 - (weekCount*7 - 7 + dayOfWeek), to: today)
            //toTodayCount 距离今天的天数 比如昨天 那么就是 -1
            let dict = NSDictionary.init(dictionary: ["date":tempDate ?? Date(),"toTodayCount":toTodayCount])
            return dict
        }
        return days
    }

    func add(component: Calendar.Component, value: Int) -> Date {
        return Calendar.current.date(byAdding: component, value: value, to: self)!
    }

    var startOfDay: Date {
        return Calendar.current.startOfDay(for: self)
    }

    var endOfDay: Date {
        return self.set(hour: 23, minute: 59, second: 59)
    }

    //获取是星期几
    func getDayOfWeek() -> HPDBaseDayOfWeek {
        let weekDayNum = Calendar.current.component(.weekday, from: self)
        let weekDay = HPDBaseDayOfWeek(rawValue: weekDayNum)!
        return weekDay 
    }

    func getTimeIgnoreSecondsFormat() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        return formatter.string(from: self)
    }
     

    static func daysBetween(start: Date, end: Date, ignoreHours: Bool) -> Int {
        let startDate = ignoreHours ? start.startOfDay : start
        let endDate = ignoreHours ? end.startOfDay : end
        return Calendar.current.dateComponents([.day], from: startDate, to: endDate).day!
    }

    static let components: Set<Calendar.Component> = [.year, .month, .day, .hour, .minute, .second, .weekday]
    private var dateComponents: DateComponents {
        return  Calendar.current.dateComponents(Date.components, from: self)
    }

    var year: Int { return dateComponents.year! }
    var month: Int { return dateComponents.month! }
    var day: Int { return dateComponents.day! }
    var hour: Int { return dateComponents.hour! }
    var minute: Int { return dateComponents.minute! }
    var second: Int { return dateComponents.second! }

    var weekday: Int { return dateComponents.weekday! }

    func set(year: Int?=nil, month: Int?=nil, day: Int?=nil, hour: Int?=nil, minute: Int?=nil, second: Int?=nil, tz: String?=nil) -> Date {
        let timeZone = Calendar.current.timeZone
        let year = year ?? self.year
        let month = month ?? self.month
        let day = day ?? self.day
        let hour = hour ?? self.hour
        let minute = minute ?? self.minute
        let second = second ?? self.second
        let dateComponents = DateComponents(timeZone: timeZone, year: year, month: month, day: day, hour: hour, minute: minute, second: second)
        let date = Calendar.current.date(from: dateComponents)
        return date!
    }
    
   
}

//extension CGFloat {
//
//    func toDecimal1Value() -> CGFloat {
//        return (self * 10).rounded() / 10
//    }
//
//}
