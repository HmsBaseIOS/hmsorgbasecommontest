//
//  HPDBaseUIFactory.swift
//  HmsBaseComponent
//
//  Created by 虞振兴 on 2023/8/14.
//

import UIKit

open class HPDBaseUIFactory: NSObject {
    //MARK: --  创建label
    public class func createLabel(frame:CGRect = .zero,text:String? = nil,font:UIFont?,color:UIColor,textAlign:NSTextAlignment = .left,breakMode:NSLineBreakMode = NSLineBreakMode.byTruncatingTail,numberOfLines:Int = 1,backgroundColor:UIColor? = nil,cornerRadius:CGFloat = 0.0,borderWidth:CGFloat = 0,borderColor:UIColor? = nil) -> UILabel{
        let lab = UILabel(frame: frame)
        lab.textColor = color;
        lab.font = font;
        lab.textAlignment = textAlign;
        lab.lineBreakMode = breakMode
        lab.numberOfLines = numberOfLines
        if(text != nil){
            lab.text = text;
        }
        if(backgroundColor != nil){
            lab.backgroundColor = backgroundColor;
        }
        if(cornerRadius>0.0){
            lab.layer.cornerRadius = cornerRadius
            lab.layer.masksToBounds = true
        }
        if(borderWidth>0.0){
            lab.layer.borderWidth = borderWidth
        }
        if(borderColor != nil){
            lab.layer.borderColor = borderColor?.cgColor
        }
        return lab
    }
    
    //MARK: --  创建textfield
    public class func createTextField(frame:CGRect = .zero,placeHolder:String?,placeHolderColor:UIColor = UIColor(hex: "C8C9CC"),font:UIFont?,color:UIColor,textAlign:NSTextAlignment = .left,backgroundColor:UIColor? = nil,cornerRadius:CGFloat = 0.0) -> UITextField{
        let temp = UITextField(frame: frame)
        temp.textColor = color;
        temp.font = font;
        temp.textAlignment = textAlign;
        if(placeHolder != nil){
            temp.placeholder = placeHolder;
            // 一般来说 一个app里的 输入框默认字体颜色是 一致的  字体大小一般是正文字体大小
            let placeDict = [NSAttributedString.Key.foregroundColor: placeHolderColor,NSAttributedString.Key.font: font] as? [NSAttributedString.Key : Any]
            temp.attributedPlaceholder = NSAttributedString.init(string: placeHolder!, attributes: placeDict)
        }
        
        temp.tintColor = UIColor.systemBlue
        if(backgroundColor != nil){
            temp.backgroundColor = backgroundColor
        }
        if(cornerRadius>0.0){
            temp.layer.cornerRadius = cornerRadius
            temp.layer.masksToBounds = true
        }
        return temp
    }
    
    
    //MARK: --  创建imageview
    public class func createImageView(frame:CGRect = .zero,contentMode:UIView.ContentMode = .scaleAspectFit,imageName:String? = nil,cornerRadius:CGFloat = 0.0) -> UIImageView{
        let imgView = UIImageView(frame: frame)
        imgView.contentMode = contentMode
        if(cornerRadius>0.0){
            imgView.layer.cornerRadius = cornerRadius
            imgView.layer.masksToBounds = true
        }
        imgView.clipsToBounds = true
        if(imageName != nil){
            imgView.image = UIImage(named: imageName!)
        }
        return imgView
    }
    
    //MARK: --  创建uiview
    public class func createView(frame:CGRect = .zero,backgroundColor:UIColor?,cornerRadius:CGFloat = 0.0,borderWidth:CGFloat = 0,borderColor:UIColor? = nil) -> UIView{
        let view = UIView(frame: frame)
        if(cornerRadius>0.0){
            view.layer.cornerRadius = cornerRadius
            view.layer.masksToBounds = true
        }
        if(backgroundColor != nil){
            view.backgroundColor = backgroundColor
        }
        if(borderWidth>0.0){
            view.layer.borderWidth = borderWidth
            view.layer.masksToBounds = true
        }
        if(borderColor != nil){
            view.layer.borderColor = borderColor?.cgColor
        }
        return view
    }
    
    //MARK: --  创建uiview 分割线
    public class func createDefaultLineView(frame:CGRect = .zero,cornerRadius:CGFloat = 0.0,borderWidth:CGFloat = 0,borderColor:UIColor? = nil) -> UIView{
        let view = UIView(frame: frame)
        if(cornerRadius>0.0){
            view.layer.cornerRadius = cornerRadius
            view.layer.masksToBounds = true
        }
        view.backgroundColor = UIColor(hex: "#E7E7E7")
       
        if(borderWidth>0.0){
            view.layer.borderWidth = borderWidth
        }
        if(borderColor != nil){
            view.layer.borderColor = borderColor?.cgColor
        }
        return view
    }
    
    //MARK: --  创建button
//    class func createButton(normalTitle:String?,font:UIFont,color:UIColor?) -> UIButton{
//        return self.createButton(normalTitle: normalTitle, selectTitle: normalTitle, font: font, color: color, selectColor: color,cornerRadius: 0)
//    }
//    class func createButton(normalTitle:String?,font:UIFont,color:UIColor?,cornerRadius:CGFloat) -> UIButton{
//        return self.createButton(normalTitle: normalTitle, selectTitle: normalTitle, font: font, color: color, selectColor: color,cornerRadius: cornerRadius)
//    }
    
    public class func createButton(frame:CGRect = .zero,normalTitle:String?,selectTitle:String? = nil ,font:UIFont?,color:UIColor?,selectColor:UIColor? = nil,cornerRadius:CGFloat = 0,backgroundColor:UIColor? = nil,borderWidth:CGFloat = 0,borderColor:UIColor? = nil) -> UIButton{
        let button:UIButton = UIButton(type: .custom)
        //禁用高亮状态
        button.adjustsImageWhenHighlighted = false
        
        button.frame = frame
        if(normalTitle != nil){
            button.setTitle(normalTitle, for: .normal)
        }
        if(!selectTitle.isNilOrEmpty){
            button.setTitle(selectTitle, for: .selected)
        }
        if(color != nil){
            button.setTitleColor(color, for: .normal)
        }
        
        if(selectColor != nil){
            button.setTitleColor(selectColor, for: .selected)
        }
        
        button.titleLabel!.font = font
        if(cornerRadius>0.0){
            button.layer.cornerRadius = cornerRadius
            button.layer.masksToBounds = true
        }
        if(backgroundColor != nil){
            button.backgroundColor = backgroundColor
        }
        if(borderWidth>0.0){
            button.layer.borderWidth = borderWidth
        }
        if(borderColor != nil){
            button.layer.borderColor = borderColor?.cgColor
        }
        return button
    }
    
    
    public class func createButtonImg(frame:CGRect = .zero,normalTitle:String? = nil,selectTitle:String? = nil ,font:UIFont? = nil,color:UIColor? = nil,selectColor:UIColor? = nil,imgeName:String? = nil,selectImgeName:String? = nil,imgeNameBackg:String? = nil,selectImgeNameBackg:String? = nil,cornerRadius:CGFloat = 0,backgroundColor:UIColor? = nil,borderWidth:CGFloat = 0,borderColor:UIColor? = nil) -> UIButton{
        let button:UIButton = UIButton(type: .custom)
        button.frame = frame
        //禁用高亮状态
        button.adjustsImageWhenHighlighted = false
        if(imgeName != nil){
            button.setImage(UIImage(named: imgeName ?? ""), for: .normal)
        }
        if(selectImgeName != nil){
            button.setImage(UIImage(named: selectImgeName ?? ""), for: .selected)
        }
        if(imgeNameBackg != nil){
            button.setBackgroundImage(UIImage(named: imgeNameBackg ?? ""), for: .normal)
//            button.setBackgroundImage(UIImage(named: imgeNameBackg ?? ""), for: .highlighted)
        }
        if(selectImgeNameBackg != nil){
            button.setBackgroundImage(UIImage(named: selectImgeNameBackg ?? ""), for: .selected)
        }
        if(cornerRadius>0.0){
            button.layer.cornerRadius = cornerRadius
            button.layer.masksToBounds = true
        }
        if(backgroundColor != nil){
            button.backgroundColor = backgroundColor
        }
        if(borderWidth>0.0){
            button.layer.borderWidth = borderWidth
        }
        if(borderColor != nil){
            button.layer.borderColor = borderColor?.cgColor
        }
         
        return button
    }
    

}
