//
//  HPDBaseCommonUtil.swift
//  HmsBaseComponent
//
//  Created by 虞振兴 on 2023/8/14.
//

import UIKit
import AVFoundation
import SystemConfiguration
import CoreTelephony

open class HPDBaseCommonUtil: NSObject {
    //any转jsonstring
    public  class func toolsChangeToJson(info: Any?) -> String{
        if(info == nil) {
            return ""
        }
        //是字符串就直接返回
        if let infoStr = info as? String  {
            return infoStr
        }
        //首先判断能不能转换
        guard JSONSerialization.isValidJSONObject(info as Any) else {
            //            PrintDebug("json转换失败")
            return ""
        }
        //如果设置options为JSONSerialization.WritingOptions.prettyPrinted，则打印格式更好阅读
        let jsonData = try? JSONSerialization.data(withJSONObject: info as Any, options: [])
        
        if let jsonData = jsonData {
            let str = String(data: jsonData, encoding: String.Encoding.utf8)
            return str ?? ""
        }else {
            return ""
        }
    }
    
    //MD5
    //isCommon 公司规定需要的额外拼接字符串
    public class func MD5String(str:String,isCommon:Bool = true) -> String{
        let needStr = String(format: "%@%@", str,isCommon ? "CdyyHMS202211" : "")
        //        print("MD5----\(needStr.md5())")
        return needStr.md5().lowercased()
    }
    
    //随机数字母和数字
    public class  func getRandomStringWithNum(num:Int) -> String {
        var string = ""
        var i = 0
        while i<num {
            i+=1
            let number = arc4random() % 36
            if number < 10{
                let figure = arc4random() % 10;
                let tempString = String(figure)
                string = string + tempString
            } else {
                let figure = (arc4random() % 26) + 97;
                let character = Character(UnicodeScalar(figure)!)
                let tempString = String(character)
                string = string + tempString
            }
        }
        return string
    }
    
    //获取oss文件 文件名称由 （当前的时间戳 + 16位由数字和字母组合出来的随机数） 组合出来的值然后进行MD5，最后得到的值来作为文件名称
    public class func ossFileNameRandomMD5() -> String {
        let timeInt = Date().timeCurrentIntervalInt()
        let randomStr = HPDBaseCommonUtil.getRandomStringWithNum(num:16)
        let temp = String(format: "%ld%@",timeInt,randomStr)
        
        return HPDBaseCommonUtil.MD5String(str:temp)
    }
    
    //MARK: -根据后台时间 返回对应文案
    public class func timeToCurrennTime(createAtstr: String) -> String {
        let  fmt = DateFormatter()
        fmt.dateFormat = "yyyy-MM-dd HH:mm:ss"
        fmt.locale = NSLocale(localeIdentifier: "en") as Locale
        
        // 2.将字符串时间,转成NSDate类型
        guard let ceateDate = fmt.date(from: createAtstr) else {
            return ""
        }
        
        let nowDate = NSDate()
        //        let interval = nowDate.timeIntervalSince(ceateDate)
        
        //                   if interval < 60 {
        //                       return "刚刚"
        //                   }
        //
        //                   if interval < (60 * 60) {
        //                   return "\(Int(interval) / 60)分钟前"
        //                   }
        //
        //                   if interval < (60 * 60 * 24){
        //                   return "\(Int(interval)/(60 * 60))小时前"
        //                   }
        //
        
        let calendar = NSCalendar.current
        //是否是今天
        if calendar.isDateInToday(ceateDate){
            fmt.dateFormat = "HH:mm"
            return fmt.string(from: ceateDate)
        }
        //昨天发生的
        if calendar.isDateInYesterday(ceateDate){
            fmt.dateFormat = "昨天 HH:mm"
            return fmt.string(from: ceateDate)
        }
        
        let gap = calendar.dateComponents([Calendar.Component.year], from: ceateDate, to: nowDate as Date)
        
        if gap.year! < 1 {
            fmt.dateFormat = "MM月dd日"
            return fmt.string(from: ceateDate)
        }
        
        fmt.dateFormat = "yyyy年MM月dd日"
        return fmt.string(from: ceateDate)
    }
    
    
    //转成 年月日 时分 的 样式
    public class func timeToyyyyMMddHHmmssTime(createAtstr: String) -> String {
        if createAtstr.count == 0 {
            return ""
        }
        let  fmt = DateFormatter()
        fmt.dateFormat = createAtstr.count>16 ? "yyyy-MM-dd HH:mm:ss" :  createAtstr.count>10 ? "yyyy-MM-dd HH:mm" : "yyyy-MM-dd"
        
        fmt.locale = NSLocale(localeIdentifier: "en") as Locale
        
        // 2.将字符串时间,转成NSDate类型
        guard let ceateDate = fmt.date(from: createAtstr) else {
            return ""
        }
        
        fmt.dateFormat = "yyyy年MM月dd日 HH:mm"
        return fmt.string(from: ceateDate)
    }
    
    //是否是图片。目前是 只有图片 或者 视频
    public class func commonCheckIsPhoto(urlStr:String) ->Bool {
        if urlStr.isNilOrEmpty() {
            return true
        }
        let url = URL(string: urlStr)
        var subFixStr = String(format: "%@",url!.pathExtension)
        subFixStr = subFixStr.lowercased()
        let photoArr = ["jpeg","png","gif","jpg"]
        //"jpeg","PNG","png","JPEG","GIF","gif"
        if photoArr.contains(subFixStr){
            return true
        }
        return false
    }
    
    //是否是视频。目前是 只有图片 或者 视频
    public class func commonCheckIsVideo(urlStr:String) ->Bool {
        if urlStr.isNilOrEmpty() {
            return true
        }
        let url = URL(string: urlStr)
        var subFixStr = String(format: "%@",url!.pathExtension)
        subFixStr = subFixStr.lowercased()
        let videoArr = kBaseAppSelectVideoTypeArr
        if videoArr.contains(subFixStr){
            return true
        }
        return false
    }
    
    //获取视频第一
    public class func getVideoFirstTimeImg(urlStr:String) -> UIImage? {
        let asset = AVURLAsset.init(url: URL.init(string: urlStr)!, options: nil)
        let gen = AVAssetImageGenerator.init(asset: asset)
        gen.appliesPreferredTrackTransform = true
        let time = CMTimeMakeWithSeconds(0, preferredTimescale:1)
        var actualTime : CMTime = CMTimeMakeWithSeconds(0,preferredTimescale:0)
        do {
            let image = try gen.copyCGImage(at: time, actualTime: &actualTime)
            return UIImage.init(cgImage: image)
        } catch  {
            return UIImage.init()
        }
    }
    
    /// 无网络返回字样
    public static var notReachable: String {
        get {
            //                return "notReachable"
            return ""
        }
    }
    
    /// 获取网络类型
    public static func getNetworkType() -> String {
        var zeroAddress = sockaddr_storage()
        bzero(&zeroAddress, MemoryLayout<sockaddr_storage>.size)
        zeroAddress.ss_len = __uint8_t(MemoryLayout<sockaddr_storage>.size)
        zeroAddress.ss_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) { address in
                SCNetworkReachabilityCreateWithAddress(nil, address)
            }
        }
        guard let defaultRouteReachability = defaultRouteReachability else {
            return notReachable
        }
        var flags = SCNetworkReachabilityFlags()
        let didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags)
        
        guard didRetrieveFlags == true,
              (flags.contains(.reachable) && !flags.contains(.connectionRequired)) == true
        else {
            return notReachable
        }
        if flags.contains(.connectionRequired) {
            return notReachable
        } else if flags.contains(.isWWAN) {
            return self.cellularType()
        } else {
            return "WiFi"
        }
    }
    
    /// 获取蜂窝数据类型
    private static func cellularType() -> String {
        let info = CTTelephonyNetworkInfo()
        var status: String
        
        if #available(iOS 12.0, *) {
            guard let dict = info.serviceCurrentRadioAccessTechnology,
                  let firstKey = dict.keys.first,
                  let statusTemp = dict[firstKey] else {
                return notReachable
            }
            status = statusTemp
        } else {
            guard let statusTemp = info.currentRadioAccessTechnology else {
                return notReachable
            }
            status = statusTemp
        }
        
        if #available(iOS 14.1, *) {
            if status == CTRadioAccessTechnologyNR || status == CTRadioAccessTechnologyNRNSA {
                return "5G"
            }
        }
        
        switch status {
        case CTRadioAccessTechnologyGPRS,
            CTRadioAccessTechnologyEdge,
        CTRadioAccessTechnologyCDMA1x:
            return "2G"
        case CTRadioAccessTechnologyWCDMA,
            CTRadioAccessTechnologyHSDPA,
            CTRadioAccessTechnologyHSUPA,
            CTRadioAccessTechnologyeHRPD,
            CTRadioAccessTechnologyCDMAEVDORev0,
            CTRadioAccessTechnologyCDMAEVDORevA,
        CTRadioAccessTechnologyCDMAEVDORevB:
            return "3G"
        case CTRadioAccessTechnologyLTE:
            return "4G"
        default:
            return notReachable
        }
    }
    
    //获取运营商信息
    public static func getCarrierName() -> String
    {
        let telephonyInfo = CTTelephonyNetworkInfo.init()
        let carrier:CTCarrier? = telephonyInfo.subscriberCellularProvider ?? nil
        let currentCountry = carrier?.carrierName ?? ""
        return currentCountry
    }
    
    //2个日期时间 进行判断 大小  要求日期格式 一致
    public static func compareTwoDateSort(firstDateStr:String?,secondDateStr:String?) -> ComparisonResult {
        
        if secondDateStr.isNilOrEmpty || firstDateStr.isNilOrEmpty {
            return ComparisonResult.orderedSame
        }
        
        //字符串集合截取 分割
        let b:CharacterSet = NSCharacterSet(charactersIn:"-, ,:,.") as CharacterSet
        let firstarr = firstDateStr!.components(separatedBy: b)
        var firstRowIntStr = ""
        for i in 0..<firstarr.count {
            let maxI:Int = Int(firstarr[i] as String) ?? 0
            firstRowIntStr.append(String(format: "%02d",maxI))
        }
        let secondarr = secondDateStr!.components(separatedBy: b)
        var secondRowIntStr = ""
        for i in 0..<secondarr.count {
            let maxI:Int = Int(secondarr[i] as String) ?? 0
            secondRowIntStr.append(String(format: "%02d",maxI))
        }
        if(Int(firstRowIntStr)! > Int(secondRowIntStr)! ){
            
            return ComparisonResult.orderedDescending
        }else if(Int(firstRowIntStr)! < Int(secondRowIntStr)! ){
            return ComparisonResult.orderedAscending
        }else{
            return ComparisonResult.orderedSame
        }
    }
    
    /// get file size
    ///
    /// - Parameter url: url
    /// - Returns: Double file size
    public class func wm_getFileSize(_ url:URL) -> Double {
        if let fileData:Data = try? Data.init(contentsOf: url,options: .mappedIfSafe) {
            let size = Double(fileData.count)
            return size
        }
        return 0.00
    }
    
    //去掉小数点后面的0
    public class func clearNumRetainZero(numberString:String) -> String{
        var outNumber = numberString
        var i = 1
        
        if numberString.contains("."){
            while i < numberString.count{
                if outNumber.hasSuffix("0") {
                    outNumber.remove(at: outNumber.index(before: outNumber.endIndex))
                    i = i + 1
                } else {
                    break
                }
            }
            if outNumber.hasSuffix("."){
                outNumber.remove(at: outNumber.index(before: outNumber.endIndex))
            }
            return outNumber
        } else {
            return numberString
        }
    }
    
    //检测 是否是 数字和字母组合
    public class func isHPDUpdatePassword(pasword : String,minCount:Int = 6,maxCount:Int = 24) -> Bool {
        
        //            let pwd =  "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,16}$"
        let pwd =  "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{\(minCount),\(maxCount)}$"
        let regextestpwd = NSPredicate(format: "SELF MATCHES %@",pwd)
        
        if (regextestpwd.evaluate(with: pasword) == true) {
            
            return true
            
        }else{
            
            return false
            
        }
        
    }
}


