//
//  HPDBaseNotiEnum.swift
//  HmsBaseComponent
//
//  Created by 虞振兴 on 2023/8/14.
//

import Foundation
open class HPDBaseRouter: NSObject {
    //获取当前显示的ViewController
    class public func getCurrentVc() -> UIViewController{
        let rootVc = UIApplication.shared.keyWindow?.rootViewController
        let currentVc = getCurrentVcFrom(rootVc!)
        return currentVc
       }

    
    class private func getCurrentVcFrom(_ rootVc:UIViewController) -> UIViewController{
     var currentVc:UIViewController
     var rootCtr = rootVc
     if(rootCtr.presentedViewController != nil) {
       rootCtr = rootVc.presentedViewController!
     }
     if rootVc.isKind(of:UITabBarController.classForCoder()) {
       currentVc = getCurrentVcFrom((rootVc as! UITabBarController).selectedViewController!)
     }else if rootVc.isKind(of:UINavigationController.classForCoder()){
       currentVc = getCurrentVcFrom((rootVc as! UINavigationController).visibleViewController!)
     }else{
       currentVc = rootCtr
     }
     return currentVc
    }
    
    //跳转到某一个页面
    class public func pushViewController(_ vc:UIViewController,animated:Bool){
        self.getCurrentVc().navigationController?.pushViewController(vc, animated: true)
    }
    
    //跳转到某一个页面
    class public func present(_ vc:UIViewController,animated:Bool){
        vc.modalPresentationStyle = .fullScreen
        HPDBaseRouter.getCurrentVc().navigationController?.present(vc, animated: animated)
    }

    //关闭当前页面
    class public func popViewController(animated:Bool){
        HPDBaseRouter.getCurrentVc().navigationController?.popViewController(animated: animated)
    }
    
    //关闭前几个页面
    //lastReduceIndex 从最后几个开始 往前关闭closeInt个页面
    class public func closeFrontController(closeInt:Int,lastReduceIndex:Int = 0) {
        //关闭前一个页面
        var temp = NSMutableArray(array: HPDBaseRouter.getCurrentVc().navigationController!.viewControllers) as! [UIViewController]
        let lastRangInt = temp.count - lastReduceIndex
        let firstRangInt = temp.count - lastReduceIndex - closeInt
        temp.removeSubrange(firstRangInt..<lastRangInt)
        HPDBaseRouter.getCurrentVc().navigationController?.viewControllers = temp
//        HPDRRouter.getCurrentVc().navigationController?.viewControllers.remove(at: (HPDRRouter.getCurrentVc().navigationController?.viewControllers.count)! - (closeInt))
         
    }
    
    
    //获取root iewController
    class public func getRootVc() -> UIViewController{
        let rootVc = UIApplication.shared.keyWindow?.rootViewController
        return rootVc!
       }
     
}
