//
//  HPDBaseNotiEnum.swift
//  HmsBaseComponent
//
//  Created by 虞振兴 on 2023/8/14.
//

import Foundation
// MARK: - 备注
// TODO: - 待完成备注
// FIXME: - 待修复警告备注

/** 屏幕宽度 */
public let kScreenWidth = UIScreen.main.bounds.size.width
/** 屏幕高度 */
public let kScreenHeight = UIScreen.main.bounds.size.height

/* 导航栏高度 固定高度 = 44.0f */
//let k_Height_NavContentBar :CGFloat  = UINavigationBar.appearance().frame.size.height
public let kHeightNavContentBar :CGFloat = 44.0
/** 状态栏高度 */
public let kHeightStatusBar :CGFloat = k_Height_statusBar()
/** 状态栏+导航栏的高度 */
public let kHeightNavigationtBarAndStatuBar: CGFloat = kHeightNavContentBar + kHeightStatusBar
/** 底部tabBar栏高度（不包含安全区，即：在 iphoneX 之前的手机） */
public let kHeightTabBar :CGFloat = 49.0
/** 底部 安全区  */
public let kHeightBottomSafe: CGFloat = k_getSafeAreaInsetsBottom()
/** 底部导航栏高度（包括安全区），一般使用这个值 */
public let kHeightTabBarAndSafe: CGFloat = kHeightBottomSafe + kHeightTabBar

//键盘输入限制配置
public let kKeyboardInputNum = "0123456789"
public let kKeyboardInputALPHA = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
public let kKeyboardInputALPHANUM = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

public let k_iphone8W = 375.0
public let k_iphone8H = 667.0

public let KScaleX:Double = Double(kScreenWidth)/k_iphone8W
public let KScaleY:Double = Double(kScreenHeight)/k_iphone8H

//行间距 默认4
public let kBaseLineSpacing:CGFloat = 5.0

public func LineX(_ l:Double) -> Double {
    return l * KScaleX
}

public func LineY(_ l:Double) -> Double {
    return l * KScaleY
}

public var kKeyWindow: UIWindow?{
    return UIApplication.shared.keyWindow
}

//页数 1 开始
public let kPageFirst:Int = 1
public let kPageDefaultCount:Int = 20  //每次分页请求数 默认20
 
// MARK: - 颜色相关
public let kBaseBackgroundColor = UIColor(hex: "#F5F5F5")

// MARK: - 字体相关
public let FONT_NAME_BOLD = "PingFangSC-Semibold"
public let FONT_NAME_MEDIUM = "PingFangSC-Medium"
public let FONT_NAME_REGULAR = "PingFangSC-Regular"
public let FONT_NAME_LIGHT = "PingFangSC-Light"
public let FONT_NAME_SFTB = "San Francisco Text-Bold"
public let FONT_NAME_SHSCNB = "Source Han Serif CN-Bold"


// MARK: - 尺寸信息： -
/* 状态栏高度  20 或 44 */
// 然而从iOS 14开始，全面屏iPhone的状态栏高度不一定是 44 或 20 了，比如下面就是这些设备在iOS 14.1上的状态栏高度。（还可能时47、48）
/// ①、顶部状态栏高度（包括安全区）
public func k_Height_statusBar() -> CGFloat {
    var statusBarHeight: CGFloat = 0;
    if #available(iOS 13.0, *) {
        let scene = UIApplication.shared.connectedScenes.first;
        guard let windowScene = scene as? UIWindowScene else {return 0};
        guard let statusBarManager = windowScene.statusBarManager else {return 0};
        statusBarHeight = statusBarManager.statusBarFrame.height;
    } else {
        statusBarHeight = UIApplication.shared.statusBarFrame.height;
    }
    return statusBarHeight;
}
/// ②、顶部安全区高度 k_Height_safeAreaInsetsTop
public func k_Height_safeAreaInsetsTop() -> CGFloat {
    if #available(iOS 13.0, *) {
        let scene = UIApplication.shared.connectedScenes.first;
        guard let windowScene = scene as? UIWindowScene else {return 0}; // guard：如果 expression 值计算为false，则执行代码块内的 guard 语句。(必须包含一个控制语句: return、 break、 continue 或 throw。)。as?：类型转换，(还有这两种：as、as!)
        guard let window = windowScene.windows.first else {return 0};
        return window.safeAreaInsets.top;
    } else if #available(iOS 11.0, *) {
        guard let window = UIApplication.shared.windows.first else {return 0};
        return window.safeAreaInsets.top;
    }
    return 0;
}


/// ③、底部安全区高度
public func k_getSafeAreaInsetsBottom() -> CGFloat {
    if #available(iOS 13.0, *) {
        let scene = UIApplication.shared.connectedScenes.first;
        guard let windowScene = scene as? UIWindowScene else {return 0};
        guard let window = windowScene.windows.first else {return 0};
        return window.safeAreaInsets.bottom;
    } else if #available(iOS 11.0, *) {
        guard let window = UIApplication.shared.windows.first else {return 0};
        return window.safeAreaInsets.bottom;
    }
    return 0;
}



public func PrintDebug<T>(_ message:T, file: String = #file, methodsName: String = #function, line: Int = #line){
    #if DEBUG
    let fileName = (file as NSString).lastPathComponent
    print("class: \(fileName)\nmethod: \(methodsName)\nline: \(line)\n\(message)\n")
    #endif
}
  

//APP支持的视频格式
public let kBaseAppSelectVideoTypeArr:Array<String> =  ["mkv","mp4","3gp","webm","mov"]


// MARK: - 枚举
public enum HPDBaseDayOfWeek: Int {
    case Sunday = 1, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday
}


